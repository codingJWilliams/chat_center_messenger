const strikethrough = require("strikethrough")
const discord = require("discord.js")
const Message = discord.Message;
const fs = require("fs")
const axios = require("axios")
function downloadInternal(url, filename) {
    return new Promise((resolve, reject) => {
        var response = axios({
            method: "GET",
            url: url,
            responseType: "stream"
        }).then(response => { 
            response.data.pipe(fs.createWriteStream("tmp/" + filename))
            response.data.on("end", async () => {
                resolve()
            })
         })
    })
}

function wait(ms) { return new Promise(x => setTimeout(x, ms)) }

async function download(attachment) {
    var fn = Date.now()
    await downloadInternal(attachment.url, fn + "-" + attachment.filename)
    await wait(500)
    return "tmp/" + fn + "-" + attachment.filename
}


module.exports = async (apis, client) => {
    client.on("message",
      async message => {
        if (message.channel.type !== "text") return;
        if (message.author.id == client.user.id) return;
        var m = JSON.parse(message.channel.topic)
        if (m.type !== "messenger") return


        if (!apis.hasOwnProperty(message.author.id)) {
            return await message.channel.send("You don't have a messenger account linked. To be able to speak, type `-link <email> <password>` in a dm to the bot. They're hashed and stored securely, pinky promise :P")
        }
        var api = apis[message.author.id]

        var c = message.content

        c = c.split("~~").map( (v, i) => {
            if (i % 3 === 1 ) {
                return strikethrough(v)
            }
            return v
        }).join("")
        var at = Array.from(message.attachments.values())
        
        var filenames = [];
        for (var x = 0; x < at.length; x++) filenames.push(await download(at[x]))
        var msg = {
            body: c,
            attachment: filenames.map( f => fs.createReadStream(f) )
        }

        api.sendMessage(msg, m.thread)
    })
}