const {promisify} = require("util")
const axios = require("axios")
const fs = require("fs")
const discord = require("discord.js")
module.exports = async function (api, client) {
    api.listen(async function (error, event) {
        if (error) return console.error(error)
        var guild = client.guilds.get("469899138125922316")
        if (event.type == "message") {
            var sender = (await promisify(api.getUserInfo)(event.senderID))[event.senderID]
            guild.channels
                .filter( (channel) => channel.type == "text")
                .filter( (channel) => JSON.parse(channel.topic).type == "messenger")
                .filter( (channel) => JSON.parse(channel.topic).thread == event.threadID)
                .forEach((channel) => {
                    channel.send(`\n**${sender.name}**: ${event.body}`)
                    if (event.attachments.length > 0) {
                        event.attachments.forEach(async attach => {
                            if (attach.type == "photo") {
                                var response = await axios({
                                    method: "GET",
                                    url: attach.url,
                                    responseType: "stream"
                                })
                                var fn = Date.now()
                                response.data.pipe(fs.createWriteStream("tmp/" + fn + ".png"))
                                response.data.on("end", async () => {
                                    await channel.send(new discord.Attachment("tmp/" + fn + ".png"))
                                })
                            }
                        })
                    }
                })
        }
    })
}