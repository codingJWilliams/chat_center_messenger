const { promisify } = require("util")
const login = promisify(require("facebook-chat-api"))
const fs = require("fs")
const discord = require("discord.js")
const config = require("./config.json")
const messengerToDiscord = require("./messengerToDiscord")
const discordToMessenger = require("./discordToMessenger")


function wait(ms) { return new Promise(x => setTimeout(x, ms)) }

async function main() {
    var logins = JSON.parse(fs.readFileSync("./logins.json", "utf-8"))
    var apis = {}
    for (var x = 0; x < logins.length; x++) {
        var login_ = logins[x]
        console.log(login_)
        apis[login_.discord] = await login({ email: login_.email, password: login_.password})
        await wait(2000)
    }    
    logins.forEach(a => apis[a.discord].setOptions({ listenEvents: true }))
    var client = new discord.Client()
    client.login(config.token)
    messengerToDiscord(apis["193053876692189184"], client)
    discordToMessenger(apis, client)
    client.on("message", async message => {
        if (message.channel.type !== "dm") return;
        if (message.content.split(" ").length < 3) return message.channel.send("Usage: -link email password")
        var login_ = {
            discord: message.author.id,
            email: message.content.split(" ")[1],
            password: message.content.split(" ").slice(2).join(" ")
        }
        logins.push(login_)
        fs.writeFileSync("./logins.json", JSON.stringify(logins))
        message.reply("Linked, next time the bot is restarted you'll be able to join the conversation.")
    })
}

main()